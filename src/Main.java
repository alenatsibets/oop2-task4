import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        DataBase base = new DataBase();
        base.add(new Book(1001, "Ashes and Stones", "Allyson Shaw", "Hodder & Stoughton", 2023, 400, 22.81, "Paperback"));
        base.add(new Book(1002, "Winters in the World", "Eleanor Catherine Parker", "Reaktion Books", 2022, 240, 19.70, "Hardback"));
        base.add(new Book(1003, "House of Sky and Breath", "Sarah J. Maas", "Bloomsbury Publishing", 2022, 805, 24.31, "Hardback"));
        base.add(new Book(1004, "Conquered", "Eleanor Catherine Parker", "Bloomsbury Publishing", 2022, 272, 24.31, "Hardback"));
        base.add(new Book(1005, "These Violent Delights", "Chloe Gong", "Hodder & Stoughton", 2011, 449, 12.86, "Paperback"));

        System.out.println("Enter 1 to see a list of books by a given author.");
        System.out.println("Enter 2 to see a list of books published by a given publisher.");
        System.out.println("Enter 3 to see a list of books released after a given year.");
        Scanner sc = new Scanner(System.in);
        int number = sc.nextInt();
        if (number == 1){
            base.listByAuthor();
        } else if (number == 2) {
            base.listByPublisher();
        } else if (number == 3) {
            base.listAfterYear();
        } else {
            throw new RuntimeException("Incorrect value");
        }

    }
}