import java.util.Scanner;

public class DataBase {
    private final Book[] books;
    private int length;

    public DataBase() {
        this.books = new Book[5];
        this.length = 0;
    }
    public void add(Book a){
        books[length] = a;
        length++;
    }
    public  void listByAuthor(){
        Scanner sc = new Scanner(System.in);
        String author = sc.nextLine();
        for (int i = 0; i < 5; i++){
            if (books[i].getAuthor().equals(author)){
                System.out.println(books[i]);
            }
        }
    }
    public void listByPublisher(){
        Scanner sc = new Scanner(System.in);
        String publisher = sc.nextLine();
        for (int i = 0; i < 5; i++){
            if (books[i].getPublisher().equals(publisher)){
                System.out.println(books[i]);
            }
        }
    }
    public void listAfterYear(){
        Scanner sc = new Scanner(System.in);
        int year = sc.nextInt();
        for (int i = 0; i < 5; i++){
            if (books[i].getYearOfPublication() >= year){
                System.out.println(books[i]);
            }
        }
    }
}
